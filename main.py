#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cv2
import numpy
import os
import sys

PEOPLE = ['丁乃麟', '吳萼洋', '柯P', '高+瑜', '陳78']

FIXED_SIZE = (200, 200)


def cv_read(file_path):
    img = numpy.fromfile(file_path, dtype=numpy.uint8)
    cv_img = cv2.imdecode(img, cv2.IMREAD_GRAYSCALE)
    return cv_img


def detect_face(img):
    cascade_path = cv2.data.haarcascades + 'haarcascade_frontalface_default.xml'
    face_cascade = cv2.CascadeClassifier(cascade_path)
    faces = face_cascade.detectMultiScale(img, scaleFactor=1.2, minNeighbors=5)
    if len(faces) == 0:
        return None
    (x, y, w, h) = faces[0]
    return img[y: y+w, x: x+h]


def get_faces_and_labels(working_dir):
    faces = []
    labels = []
    for label, name in enumerate(PEOPLE):
        name_dir = working_dir + '/training_data/' + name
        images = os.listdir(name_dir)
        for filename in images:
            if filename.startswith("."):
                continue
            file_path = name_dir + '/' + filename
            cv_img = cv_read(file_path)
            face = detect_face(cv_img)
            if face is not None:
                face = cv2.resize(face, FIXED_SIZE)
                faces.append(face)
                labels.append(label)
    return faces, labels


def lbph_training(faces, labels):
    # face_recognizer = cv2.face.EigenFaceRecognizer_create()
    # face_recognizer = cv2.face.FisherFaceRecognizer_create()
    face_recognizer = cv2.face.LBPHFaceRecognizer_create()
    face_recognizer.train(numpy.asarray(faces), numpy.asarray(labels))
    return face_recognizer


def predict(img, face_recognizer):
    face = detect_face(img)
    if face is None:
        return 'no face data'
    face = cv2.resize(face, FIXED_SIZE)
    label, confidence = face_recognizer.predict(face)
    if label == -1:
        return 'unknown'
    return PEOPLE[label]


def main():
    working_dir = os.path.dirname(os.path.realpath(__file__))
    faces, labels = get_faces_and_labels(working_dir)
    face_recognizer = lbph_training(faces, labels)
    test_data_dir = working_dir + '/testing_data'
    test_images = os.listdir(test_data_dir)
    for filename in test_images:
        if filename.startswith("."):
            continue
        image_path = test_data_dir + '/' + filename
        cv_img = cv_read(image_path)
        cv2.imshow("testing image", cv_img)
        cv2.waitKey(0)
        predict_result = predict(cv_img, face_recognizer)
        print(predict_result)
        cv2.destroyAllWindows()


if __name__ == '__main__':
    main()
